#!/bin/bash

echo "ingrese la ruta del directorio"
read  DIR

echo $DIR

if [[ $DIR = " "]]; then
    echo "Se requiere de un directorio para listar"
    echo "La forma de uso es la siguiente bash ordena.bash directorio"
    exit 1> exits.txt
else
    cd $DIR
    ls -lSr |more > lista.txt
    awk -F " " '{print $9}' lista.txt
fi
