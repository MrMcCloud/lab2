#!/bin/bash

echo "introduzca un nombre para el directorio a crear"
read DIR

if [[ ! -d $DIR ]]; then
    mkdir $DIR
    cp temp.txt /$PWD/$DIR
    cd $DIR
    for i in $(cat temp.txt) ; do
        touch $i
    done 
    rm -f temp.txt
    ls
fi
